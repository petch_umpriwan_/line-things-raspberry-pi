# LINE Things with Raspberry Pi

**I choose to put this on Bitbucket because I need some privacy during LINE DEV RECRUITMENT event. I'll put this on my [Github](https://github.com/petchumpriwan) later after the event.**

These are the codes for LINE Things Developer Trial to use with Raspberry Pi using Node.js.

## Table of Contents
- [Getting Start](#getting-started)
- [Development Note](#development-note)

## Getting Started
1. Wire the Pi's GPIO like this image.
![](./images/wiring.png)
2. Clone this repository.
3. Install Node.js version 8.x, open terminal and run this code
    > If you have higher version please downgrade to 8.x or Bleno might fail to install.
    ```
    curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
    sudo apt-get install -y nodejs
    ```
3. Install this code in terminal.

    ```
    sudo apt-get install bluetooth bluez libbluetooth-dev libudev-dev
    ```

4. Run `npm install` to install all requirements.
5. Run `sudo npm start` to start bluetooth service.
    > If you want to see what happenning under the hood, run `sudo npm run debug`.
6. Scan the following QR code with LINE to activate LINE Things.

    ![](https://developers.line.biz/media/line-things/qr_code-311f3503.png)

    Upon turning the device on, a device with the name `LINE Things Starter (Default Firmware)`  will be detected.

    Once the device is connected, the LIFF app can then be launched by selecting the device.

7. Now you can with LED and Button. Control LED over the LIFF app or push button on board and see what happen on LIFF app.

    ![](./images/demo.gif)

---

# Development Note

## Language - Node.js
I choose Node.js because I have used [Bleno](https://github.com/noble/bleno) library before to deal with Raspberry Pi's BLE. It has all I need to implement the LINE Things Trial Hardware as required in the project from simple services to secure function right out of the box. Moreover, In the view of project distribution, I think Node.js is doing good, Setting up Node.js ecosystem is easy. It has fewer installation complications. Since I have around 2 days to complete the project, I choose to go with tools I familiar with. However, I faced the problem with its secure pairing which I will mention it at the end of this note.

## Simulation Choice - LED & Button

### Write Characteristic
I go with a physical device which is LED for write characteristic because it can represent how it looks as IoT device better than do a console log on terminal screen. However logging still important for debugging, so, I put it in [Debug](https://github.com/visionmedia/debug). It easier to choose what to show and when to show the log.

### Notify Characteristic
Since I add LED to Raspberry Pi the notify characteristic can't be others but a button. It helps me to easier control the notification. I don't have to type more on console to send notifications, just push it and button on LIFF app just change state immediately. It also has debugging function just the same with LED.

## Problems with Securities

From the requirement on project which requires that all characteristics must be secured and encrypted with a minimum of no MITM prevention. It means that I need at least Raspberry Pi pairing with my mobile.

Since I use iPhone, iOS needs the peripheral bonding capability during the pairing process and iOS won't initiate pairing by itself but require peripherals to request for pairing capabilities which initiated programmatically by reading or writing to a secured GATT characteristic. So, I put secure requirement on my GATT characteristic.

```javascript
// index.js

const psdiCharacteristic = new Characteristic({
  uuid: PSDI_CHARACTERISTIC_UUID,
  properties: ['read'],
  secure: ['read'],  // Secure Requirement.
  onReadRequest: (offset, callback) => {
    const result = Characteristic.RESULT_SUCCESS
    const data = new Buffer.from(bleno.address)
    debugBLE('User Read : psdiCharacteristic = ' + data)
    callback(result, data)
  }
})
```
I tried to read the psdiCharacteristic from my iPhone (using LightBlue app for manual access), it comes up with pairing request as expected.

![](./images/iospairstep.png)

But after pairing the value of psdiCharacteristic doesn't come up. So, I start finding what happen inside. After loging the pairing event I found that SMP pairing is failed due to invalid long term keys was generated from bleno.

![](./images/ltkfail.png)

It needs more time to address why the LTKs is invalid. I also saw that other people was facing with this problem too and it sill on issue page ([1](https://github.com/noble/bleno/issues/339), [2](https://github.com/noble/bleno/issues/187)).


I admit that I can't fulfilled the requirement in this time but if possible, I looking forward to discuss about the solutions of this problem.

Best regards,

Witchanon Umpriwon

petch.umpriwan@gmail.com
